package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class GeneralController {

    @GetMapping(value = "/holamundo")
    public String holamundo(){
        return "Hola mundo";
    }
}
